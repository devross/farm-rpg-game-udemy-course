﻿using UnityEngine;
using Cinemachine;

public class SwitchConfineBoundingShape : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        SwitchBoundingShape(); 
    }

    /// <summary>
    /// Switch the collider that cinemachine uses to define the edged of the screen
    /// </summary>
    private void SwitchBoundingShape()
    {
        // Gets the polygon collider on the 'boundsconfiner' gameobject which is used by Cinemachine to preventthe camera going beyond the screen edges
        PolygonCollider2D polygonCollider2D = GameObject.FindGameObjectWithTag(Tags.BoundsConfiner).GetComponent<PolygonCollider2D>();

        CinemachineConfiner cinemachineConfiner = GetComponent<CinemachineConfiner>();

        cinemachineConfiner.m_BoundingShape2D = polygonCollider2D;

        // Since the confiner bounds have changedm need to call this to clear the cashe

        cinemachineConfiner.InvalidatePathCache();
    }
}
